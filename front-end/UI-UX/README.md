# Training UI/UX

### User Interface 

  - **call to action buttons**: daca se doreste ca utilizatorul sa intreprinda o actiune ( e.g. download, click to a form) este de preferat ca acel buton sa fie colorat diferit/ sa fie scos in evidenta intr-un fel sau altul 

    ![](https://conversionxl.com/wp-content/uploads/2019/04/amazon-add-to-cart-button.png)
    *Butoanele de "Add to Cart" si "Buy now" de la Amazon ies mai mult in evidenta, deoarece asta este scopul website-ului: sa faca lumea sa cumpere cat mai mult de la ei. De aceea, in functie de scop si actiunile pe care dorim sa le intreprinda utilizatorul, se pot colora anumite butoane mai diferit, astfel incat sa iasa in evidenta.*

  - **spatiu alb**: este un respiro, un spatiu care face ca elementele de pe pagina sa nu fie incarcarcate (e.g. [Ling's cars](https://www.lingscars.com/)); asta se poate realiza prin margins, gutters 

  - **legea proximitatii si cea a similaritatii**: Oamenii au tendinta sa grupeze lucrurile care sunt apropiate în spațiu. Ele devin un singur obiect perceput. La fel grupam si lucrurile care seamana: forma, culoare, umbra si altele; (e.g. card-urile de la ITFest)
  
    ![](https://conversionxl.com/wp-content/uploads/2012/05/500px-Gestalt_proximity.svg_.png)
   *Mintea umana are tendinta de a percepe elementele apropiate ca fiind un obiect cu totul*
    
    ![](https://conversionxl.com/wp-content/uploads/2012/05/500px-Gestalt_similarity.svg_.png)
   *La fel si cu obiectele care par similare, de exeplu prin culoare*   

    ![card-uri ITFest](https://i.imgur.com/neKWioH.png)
   
    *Card-urile cu informatii de la ITFest au aceeasi culoare, aceleasi forme care se repeta, textul este asezat asemanator, iesind mai mult in evidenta numerele si fiind mai mari.*

  - **ierarhia pe pagina a elementelor**: cu cat vedem un element mai mare, cu atat ii acordam mai multa importanta, inconstient; in web design asta se poate realiza prin folosirea heading-urilor de diferite dimensiuni pentru a stabili atat o ierarhie, cat si o importanta a informatiilor

    ![](https://conversionxl.com/wp-content/uploads/2012/05/37-12051.jpg)
   *Daca am vrea sa clasificam cercurile in functie de importanta, atunci am considera cercul albastru mai important, dupa cercul verde si tot asa. La fel si in web design, unele elemente de pe pagina sunt mai importante decat altele si ies in evidenta mai bine prin marime, font, culoare si alte caracteristici. In functie de scopul site-ului nostru, anumite elemente (e.g. un buton de CUMPARA) ies mai mult in evidenta decat altele.*   

  - **ordinea in care percepem elementele**: oamenii vad prima oara pagina ca un intreg, inainte sa deosebeasca footer-ul, banner-ul, meniul etc.;    

### User Experience

  - **consistenta**: daca se decide folosirea unor culori (oricare ar fi acelea) sa se pastreze pe toate paginile site-ului; la fel si cu formele, daca exista (e.g. daca se merge pe colturi drepte, este de preferat ca toate elementele sa aibe colturi drepte, sau rotunde); de asemenea, colturile rotunde sunt preferate peste colturile drepte, deoarece acestea ne duc cu gandul la ceva ascutit, respingator, de care nu ar trebui sa ne atingem; in schimb, colturile rotunjite par mult abordabile pentru utilizatori.
  
    ![](https://i.imgur.com/oX5AAp2.png)
    *In imagine sunt prezente aceleasi culor, acelasi stil (colturile acelea), fontul este acleasi pe toate ecranele*

  - **feedback**: atunci cand utilizatorul intreprinde o actiune sa aibe o confirmare a ceea ce a facut 
  
    ![](https://i.imgur.com/ikPFtbz.png)
    *Un feedback destul de subtil este meniul care arata pe ce tab suntem, ca si utilizatorul sa stie si sa nu fie confuz*
    ![](https://assets.materialup.com/uploads/33e7e116-84da-4621-93b5-fc568d2010f7/preview.gif)
    *Exemplul clasic cu feedback dupa trimiterea unui formular, pentru ca utilizatorul sa fie sigur ca s-au trimis si sa nu apese acel buton de mai multe ori, fara niciun rezultat.*
    *De asemenea,tot in cazul formularelor, este bine sa evidentiem erorile, pentru a incuraja utilizatorul sa completeze in continuare formularul si sa duca la bun sfarsit. Pentru aceasta se pot folosi mesaje de tip Toast, cu highlight rosu pe campuri sau cu iconite rosii in dreptul campurilor gresite. Mesajele de eroare trebuie sa fie cat mai explicite si concise, un utilizator nu va citi foarte mult ca sa isi dea seama de ce este o greseala. Si daca face asta, cel mai probabil se va plictisi si va renunta in a mai completa formularul -> client pierdut -> afacerea/evenimentul are de suferit.*

  - **scopul elementelor**: fiecare element ar trebui sa aibe un scop pentru care este asezat in pagina (fie ca este un call to action button, fie ca este un float menu)
  - **numarul de pasi**: daca exista prea multe meniuri prin care utilizatorul trebuie sa navigheze pentru a ajunge la informatia care il intereseaza, devine obositor si cel mai probabil va parasi site-ul, fara sa il exploreze (nr de click-uri este important, se pot obtine bani dn asta) si probabil si fara sa fi gasit informatia pe care o cauta 
  - **context**: de unde acceseaza utilizatorul site-ul tau? (mobil vs desktop)
  
    ![](https://i.imgur.com/QjpENmK.png)
    *Un studiu din anul 2018 arata ca 87% dintre utilizatori folosesc mai mult telefonul mobil pentru a accesa internetul. De aceea, se incearca din ce in ce mai mult un design mobile-first, pentru a ne asigura ca pe mobil se aliniaza bine lucrurile. Realizand site-ul in acest mod, ele se vor incarca si mai repede, fiind optimizate pentru acest tip de platforma*
  - **contrastul culorilor**: este necesar pentru a putea citi mai bine informatiile de pe site si pentru a creste [SEO-ul](https://moz.com/learn/seo/what-is-seo) 
  
    ![](https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/42b8f813-c31b-4a05-9658-08707d3f5eae/fueled-smashing-4-preview-opt.png)
    ![](https://stephaniewalter.design/wp-content/uploads/2014/04/accessible-colorcode-00.jpg)

### The worst mf form. The outlaw form. Check it out [here](https://userinyerface.com/).&nbsp;

> Simple websites are scientifically better

**Useful links:
[8 Web Design Principles to Know in 2019](https://conversionxl.com/blog/universal-web-design-principles/amp/)
[16 Important UX Design Principles for Newcomers](https://www.springboard.com/blog/ux-design-principles/)
------


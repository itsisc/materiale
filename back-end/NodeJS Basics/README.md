# NodeJs Basics

### Requirements
 - [_nodeJS_](https://nodejs.org/en/download/)
 - [_Postman_](https://www.getpostman.com/downloads/)
 - [_XAMPP_](https://www.apachefriends.org/ro/index.html)

### Steps

1.  Create a folder.
1.  Open the terminal in the folder.
1.  Initialize the project

    `npm init` 
1. Install npm required npm packages
```
    `npm install -s express` (required for the enviornment of the app)
    `npm install -s body-parser` (required for retrieving body in JSON)
    `npm install -s mysql`(required for mysql installation)
    
    Optional
    `npm install -g nodemon`(automatically restarts the server when saving the code)
 ```   
4. Create a file named server.js
5. Write this code in server.js
```
const express = require('express')   //Importing express framework in a const
const bodyParser = require('body-parser')   //Importing body-parser in a const(body-parser is used for retrieving the requests in a JSON)
const mysql = require("mysql")   //Importing mysql in a const
const app = express();   //Initializing an express instance for the app
const port = 8080    //Setting the port for the server
app.use(bodyParser.json())   // Transforming bodies of the requests in JSON object


app.listen(port, () => {
    console.log('Serverul ruleaza pe portul: ' + port)  //Turn on server function
})
```
6. Create the connection with the database
```
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "Masinutze"
});
```

7. Create the table
```
connection.connect(err => {
  console.log("Baza de date conectata!");
  const sql =
    "CREATE TABLE IF NOT EXISTS Masini(marca VARCHAR(25), cp INTEGER, culoare VARCHAR(15), capacitateMotor INTEGER)";
  connection.query(sql, err => {
    if (err) throw err;
  });
});
```

8. Create a post api to add a new record into database
```app.post("/masina", (req, res) => {
  const masina = {
    marca: req.body.marca,
    cp: req.body.cp,
    culoare: req.body.culoare,
    capacitateMotor: req.body.capacitateMotor


  };
  // Create an empty array for errors
  let errors = [];

// Check if all the fields are completed
  if (
    !masina.marca ||
    !masina.cp ||
    !masina.culoare ||
    !masina.capacitateMotor
  ) {
    errors.push("Nu ati introdus unul sau mai multe campuri");
  }
// Check if a field contains only letters
  if (!masina.marca.match("[^0-9.]")) {
    errors.push("Marca masinii trebuie sa contina doar litere");
  }

// If the error array is empty insert into database the record
  if (errors.length === 0) {
    const insert =
      "INSERT INTO Masini(marca , cp, culoare, capacitateMotor) VALUES ('" +
      masina.marca +
      "','" +
      masina.cp +
      "','" +
      masina.culoare +
      "','" +
      masina.capacitateMotor +
      "')";
    connection.query(insert, err => {
      if (err) throw err;
      else {
        console.log("Masina adaugata in bd.");
        res.status(200).send({ message: "Ai introdus o masina in bd" });
      }
    });
  } else {
    console.log("Eroare de server!");
    res.status(500).send(errors);
  }
});
```

9. Create a get request to return all the data from the database
```
app.get("/masini", (req, res) => {
    const sql = "SELECT * FROM Masini";
    connection.query(sql, (err, result) => {
    if (err) throw err;
    res.status(200).send(result);
  });
});
```
10. Save all and type in terminal npm start/nodemon

11. Open postman
    - Select the type of request you want to submit.
    - In the adress bar, type: localhost:(your port)/(your endpoint)
      Example:
     POST request on localhost:8080/masina
     GET request on localhost:8080/masini
